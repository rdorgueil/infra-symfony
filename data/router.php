<?php
/**
 * This file is a router for php 5.4 embedded web server, allowing to serve a symfony1 application.
 */

$__router__ = function () {
  if (file_exists($file = $_SERVER['DOCUMENT_ROOT'].'/'.$_SERVER['SCRIPT_NAME'])) {
    return false;
  } else {
    unset($file);
    $_SERVER['SCRIPT_NAME'] = '/index.php';
    require $_SERVER['DOCUMENT_ROOT'].'/index.php';
  }
};

return $__router__();

