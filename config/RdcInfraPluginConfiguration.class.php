<?php

class RdcInfraPluginConfiguration extends sfPluginConfiguration
{
    const UNCAUGHT_EXCEPTION_EVENT = 'rdc.uncaught_exception';

    protected $_config;
    protected $_sentry_client;

    public function initialize()
    {
        parent::initialize();

        $config_dir = sfConfig::get('sf_config_dir');
        $config_file = $config_dir.'/rdc_infra.conf.php';

        if (file_exists($config_file) && is_readable($config_file)) {
            $this->_config = include($config_file);
        }

        $this->dispatcher->connect(self::UNCAUGHT_EXCEPTION_EVENT, array($this, 'handleUncaughtException'));

        // Autoload sentry client, to register error handlers, mostly.
        $this->getSentryClient();
    }

    public function initializeAutoload()
    {
    }

    public function getSentryClient() {
        // Try to build a client object for sentry
        if ($this->_config && is_null($this->_sentry_client)) {
            if (($uri = $this->get('sentry.uri')) && $this->loadSentryLibrary()) {
                $this->_sentry_client = new Raven_Client($uri);

                $error_handler = new Raven_ErrorHandler($this->_sentry_client);
                $error_handler->registerExceptionHandler();
                $error_handler->registerErrorHandler();
                $error_handler->registerShutdownFunction();
            }
        }

        // Memorize our unability to load sentry client.
        if (is_null($this->_sentry_client)) {
            $this->_sentry_client = false;
        }

        return $this->_sentry_client;
    }

    public function handleUncaughtException(sfEvent $event) {
        $client = $this->getSentryClient();

        if ($client) {
            $e = $event->getSubject();
            $client->captureException($e);
        }
    }

    protected function get($key, $default=null) {
        $key = explode('.', $key);
        $config = $this->_config;

        while (count($key)) {
            $atom = array_shift($key);
            if (is_array($config) && array_key_exists($atom, $config)) {
                $config = $config[$atom];
            }
            else {
                return $default;
            }
        }

        return $config;
    }

    private $loaded = false;
    private function loadSentryLibrary() {
        if (!$this->loaded) {
            // Avoid buggish autoloading in certain propel tasks. Reason still mysterious.
            require_once __DIR__.'/../lib/vendor/raven-php/lib/Raven/Stacktrace.php';
            require_once __DIR__.'/../lib/vendor/raven-php/lib/Raven/Serializer.php';
            require_once __DIR__.'/../lib/vendor/raven-php/lib/Raven/Client.php';
            require_once __DIR__.'/../lib/vendor/raven-php/lib/Raven/Util.php';
            require_once __DIR__.'/../lib/vendor/raven-php/lib/Raven/Compat.php';
            require_once __DIR__.'/../lib/vendor/raven-php/lib/Raven/Processor.php';
            require_once __DIR__.'/../lib/vendor/raven-php/lib/Raven/SanitizeDataProcessor.php';
            require_once __DIR__.'/../lib/vendor/raven-php/lib/Raven/ErrorHandler.php';
        }
        return true;
    }
}
