<?php

class RdcInfraRunServerTask extends sfBaseTask {
  const DEFAULT_PORT = '4000';
  const DEFAULT_HOST = '0.0.0.0';
  protected function configure()
  {
    $this->addOptions(array(
      new sfCommandOption('listen', null, sfCommandOption::PARAMETER_OPTIONAL, 'Interface and port to bind the server to.', self::DEFAULT_HOST.':'.self::DEFAULT_PORT),
    ));

    $this->namespace = 'rdc';
    $this->name = 'runserver';
    $this->briefDescription = 'Runs the symfony application in an embedded PHP server.';

    $this->detailedDescription = <<<EOF
The [rdc:runserver|INFO] task leverage PHP 5.4 new features to run an embedded Web server for your application.:

Example usage (all 3 do the exact same):

  $ [./symfony rdc:runserver 0.0.0.0:4000|INFO]
  $ [./symfony rdc:runserver :4000|INFO]
  $ [./symfony rdc:runserver|INFO]
EOF;
  }

  /**
   * Executes the current task.
   *
   * @param array $arguments  An array of arguments
   * @param array $options    An array of options
   *
   * @return integer 0 if everything went fine, or an error code
   */
  protected function execute($arguments = array(), $options = array())
  {
    if (version_compare(phpversion(), '5.4.0', '<')) {
      $this->logSection('fatal', 'PHP embedded server is not available prior to PHP 5.4.');
      return false;
    }

    $listen_to = explode(':', $options['listen']);
    if (count($listen_to) == 2) {
      $host = $listen_to[0];
      $port = $listen_to[1];
    } elseif (count($listen_to) == 1) {
      $host = $listen_to[0];
      $port = null;
    }

    $host = $host ?: self::DEFAULT_HOST;
    $port = $port ?: self::DEFAULT_PORT;
    $cmd = '(cd '.escapeshellarg(sfConfig::get('sf_web_dir')).' && '.escapeshellcmd(PHP_BINARY).' -S '.escapeshellarg($host.':'.$port).' '.escapeshellarg(__DIR__.'/../../data/router.php').')';

    $this->logSection('listen', 'Server is now listening to '.$host.':'.$port.'.');
    exec($cmd);
  }
}