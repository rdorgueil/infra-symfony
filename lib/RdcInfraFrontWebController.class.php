<?php

class RdcInfraFrontWebController extends sfFrontWebController
{
  public function dispatch()
  {
    try {
      // reinitialize filters (needed for unit and functional tests)
      sfFilter::$filterCalled = array();

      // determine our module and action
      $request = $this->context->getRequest();
      $moduleName = $request->getParameter('module');
      $actionName = $request->getParameter('action');

      if (empty($moduleName) || empty($actionName)) {
        throw new sfError404Exception(sprintf('Empty module and/or action after parsing the URL "%s" (%s/%s).', $request->getPathInfo(), $moduleName, $actionName));
      }

      // make the first request
      $this->forward($moduleName, $actionName);
    }
    catch (sfStopException $e) {
      // This is symfony control flow. No need for logging.
    }
    catch (sfException $e) {
      $this->notifyUncaughtExceptionEvent($e);
      $e->printStackTrace();
    }
    catch (Exception $e) {
      $this->notifyUncaughtExceptionEvent($e);
      sfException::createFromException($e)->printStackTrace();
    }
  }

  protected function notifyUncaughtExceptionEvent(Exception $e)
  {
    $this->dispatcher->notify(new sfEvent($e, RdcInfraPluginConfiguration::UNCAUGHT_EXCEPTION_EVENT));
  }
}
